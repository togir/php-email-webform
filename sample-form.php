<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';


// Only process POST reqeusts. And check validity of data before connecting to the server
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Get the form fields and remove whitespace.
    $name = strip_tags(trim($_POST["name"]));
    $name = str_replace(array("\r","\n"),array(" "," "),$name);
    $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
    $message = trim($_POST["message"]);

    // Check that data was sent to the mailer.
    if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {

        // Set a 400 (bad request) response code and exit.
        http_response_code(400);
        echo "Es gab ein Problem mit dem Formular! Bitte versuchen sie es erneut.";
        exit;
    }

} else {
    // Not a POST request, set a 403 (forbidden) response code.
    http_response_code(403);
    echo "Es ist ein Fehler aufgeteten. Bitte versuchen sie es erneut!";
}


// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                    // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'mail.m2rocks.de';                      // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'usernmae';                  			// SMTP username
    $mail->Password   = 'password';                 			// SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('absenderadress', 'Kontaktformular');
    $mail->addAddress('empfänger-adresse');                   				// Adress of recipient
    //$mail->addAddress('admin@m2rocks.de');                   // multiple reciepients are possible
    $mail->addReplyTo($email);

    $mail->isHTML(true);                                        // Set email format to HTML
    $mail->Subject = 'Neue Nachricht über das Kontakformular von ' . $name;

    // Html body of the message
    $mail->Body    = 'Name:<b> '. $name .' </b> <br> Absender-Adresse: <b> ' . $email . ' </b> <br><hr> ' . $message . ' <br> <hr> Bitte Antworten sie nicht auf diese Nachricht! Benutzten Sie zum Antworten die Adresse ' . $email .' . <br>';

    // Message in plain-text
    $mail->AltBody = 'Name: ' . $name . '\n Email-Adresse: '. $email .'\n\n Nachricht:\n '. $message .'\n Bitte Antworten sie nicht auf diese Nachricht! Benutzten Sie zum Antworten die Adresse '. $email .' . \n';

    $mail->send();

    // Set a 200 (okay) response code.
    http_response_code(200);
    echo "Danke! Ihre Nachricht wurde versendet.";

} catch (Exception $e) {
    // Set a 500 (internal server error) response code.
    http_response_code(500);
    echo "Ihre Nachricht konnte nicht versendet werden! Bitte versuchen sie eine andere Kontaktmöglichkeit.";
}